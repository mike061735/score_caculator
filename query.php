<?php
    // Initialize the session
    if(!isset($_SESSION)) { 
        session_start(); 
    }
    
    // Check if the user is already logged in, if yes then redirect him to welcome page
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] !== true){
        header("location: home.php");
        exit;
    }
    
    // Include config file
    require_once "config.php";

    // Define variables and initialize with empty values
    $username = $password = "";
?>
<html>
    <head>
        <title>期末成績計算系統</title>
        <link rel="icon" href="images/favicon.ico" type="image/ico">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/home.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body style="background-image: url('images/background.jpg') !important; background-size: cover !important;">
        <?php
            require_once "nav.php";
        ?>
        <div class="wrapper fadeInDown">
            <div id="formContent" style="max-width: 950px !important;">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first mt-5">
                    <h1>查詢結果</h1>
                </div>
                <div class="fadeIn second mt-3 px-2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col" class="align-middle text-center">平時占比</th>
                                <th scope="col" class="align-middle text-center">平時成績</th>
                                <th scope="col" class="align-middle text-center">期中占比</th>
                                <th scope="col" class="align-middle text-center">期中成績</th>
                                <th scope="col" class="align-middle text-center">目前成績</th>
                                <th scope="col" class="align-middle text-center">預估期末占比</th>
                                <th scope="col" class="align-middle text-center">預估期末考試成績</th>
                                <th scope="col" class="align-middle text-center">課程名稱</th>
                                <th scope="col" class="align-middle text-center">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $userId = $_SESSION["id"];
                                $sql = "SELECT * From `score` WHERE `userId` = $userId";
                                $result = mysqli_query($link, $sql);
                                $row = $result->fetch_all();
                                if(mysqli_num_rows($result) > 0){
                                    foreach ($row as $r){
                                        $cur_score = $r[4] * $r[1] / 100 + $r[2] * $r[5] / 100;
                                        echo "<tr>";
                                        echo "<td align='center'>".$r[1]."</td>";
                                        echo "<td align='center'>".$r[4]."</td>";
                                        echo "<td align='center'>".$r[2]."</td>";
                                        echo "<td align='center'>".$r[5]."</td>";
                                        echo "<td align='center'>".$cur_score."</td>";
                                        echo "<td align='center'>".$r[3]."</td>";
                                        echo "<td align='center'>".$r[6]."</td>";
                                        //印出課程名稱
                                        $classId = $r[8];
                                        $select_class_sql = "SELECT * From `class` WHERE `id` = $classId";
                                        $class_result = mysqli_query($link, $select_class_sql);
                                        $class_row = $class_result->fetch_row();
                                        echo "<td align='center'>".$class_row[1]."</td>";
                                        echo "<td align='center'><a class='btn btn-info mb-2' href='score.php'>修改</a></td>";
                                        echo "</tr>";
                                    }
                                }else{
                                    echo "<tr>";
                                    echo "<td colspan='100%' align='center'>無資料</td>";
                                    echo "</tr>";
                                }
                            ?>
                            <!-- <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            </tr>
                            <tr> -->
                        </tbody>
                    </table>
                    <a class='btn btn-info mb-2' href='score.php'>回上一頁</a>
                </div>
            </div>

        </div>
    </div>
    </body>
</html>
<?php

?>
