<?php
    // Initialize the session
    if(!isset($_SESSION)) { 
        session_start(); 
    }
    
    // Check if the user is already logged in, if yes then redirect him to welcome page
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] !== true){
        header("location: home.php");
        exit;
    }
    
    // Include config file
    require_once "config.php";

    // Define variables and initialize with empty values
    $username = $password = "";
?>
<html>
    <head>
        <title>期末成績計算系統</title>
        <link rel="icon" href="images/favicon.ico" type="image/ico">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/home.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body style="background-image: url('images/background.jpg') !important; background-size: cover !important;">
        <?php
            require_once "nav.php";
        ?>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first mt-5">
                    <h1>計算結果</h1>
                </div>
                <div class="fadeIn second mt-5">
                    <?php
                        $userId = $_SESSION["id"];
                        $class = $_SESSION["class"];
                        $sql = "SELECT * From `score` WHERE `userId` = $userId AND `classId` = $class";
                        $select_class_sql = "SELECT * From `class` WHERE `id` = $class";
                        $result = mysqli_query($link, $sql);
                        $class_result = mysqli_query($link, $select_class_sql);
                        $row = $result->fetch_row();
                        $class_row = $class_result->fetch_row();
                        if(mysqli_num_rows($result) > 0){
                            $cur_score = $row[4] * $row[1] / 100 + $row[2] * $row[5] / 100;
                            echo "<h3>課程：".$class_row[1]."</h3>";
                            echo "<h4>平時占比：".$row[1]."% 平時成績：".$row[4]."</h4>";
                            echo "<h4>期中占比：".$row[2]."% 平時成績：".$row[5]."</h4>";
                            echo "<h4>您目前的學期成績為：$cur_score 分</h4>";
                            if($row[6] != 0){
                                echo "<br><h3 class='text-danger'>期末成績至少需要</h3>";
                                echo "<h3 class='text-danger'>".$row[6]." 分</h3>";
                                echo "<h3 class='text-danger'>學期成績才會及格喔! 加油~</h3>";
                            }else{
                                echo "<br><h3 class='text-success px-2'>恭喜您，躺著都會過喔!!!</h3>";
                            }
                        }
                    ?>
                    <a class='btn btn-info mb-2' href='score.php'>回上一頁</a>
                </div>
            </div>

        </div>
    </div>
    </body>
</html>
<?php

?>
