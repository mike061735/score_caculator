<?php
    // Initialize the session
    if(!isset($_SESSION)) { 
        session_start(); 
    }
    
    // Check if the user is already logged in, if yes then redirect him to welcome page
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
        header("location: score.php");
        exit;
    }
    
    // Include config file
    require_once "config.php";

    // Define variables and initialize with empty values
    $username = $password = "";
?>
<html>
    <head>
        <title>期末成績計算系統</title>
        <link rel="icon" href="images/favicon.ico" type="image/ico">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/home.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body style="background-image: url('images/background.jpg') !important; background-size: cover !important;">
        <?php
            require_once "nav.php";
        ?>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first mt-5">
                    <h1>登入</h1>
                </div>

                <!-- Login Form -->
                <form name="form1" method="POST">
                    <input type="number" id="username" class="fadeIn first" name="username" placeholder="請輸入學號">
                    <input type="password" id="password" class="fadeIn second" name="password" placeholder="請輸入密碼">
                    <input type="submit" class="fadeIn fourth" value="登入">
                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                <a class="underlineHover" href="register.php">還沒有帳號? 點我註冊</a>
            </div>

        </div>
    </div>
    </body>
</html>
<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty(trim($_POST["username"])) && empty(trim($_POST["password"]))){
        echo '<script>Swal.fire("每個欄位都必須填寫", "", "error")</script>';
    } else{
        $username = trim($_POST["username"]);
        $password = trim($_POST["password"]);
    }

    if(isset($username) && isset($password)){
        $sql = "SELECT * From `users` WHERE `username` = $username";
        $result = mysqli_query($link, $sql);
        // echo "<script>Swal.fire('$result', '', 'success')</script>";
        // echo isset($result);
        if(mysqli_num_rows($result) > 0){
            $row = $result->fetch_row();
            echo json_encode($row);
            if($password == $row[3]){
                // Password is correct, so start a new session
                session_start();

                // Store data in session variables
                $_SESSION["loggedin"] = true;
                $_SESSION["id"] = $row[0];
                $_SESSION["username"] = $username;
                $_SESSION["nickname"] = $row[2];


                // Redirect user to score page
                header("location: score.php");
            }else{
                echo "<script>Swal.fire('帳號或密碼錯誤', '', 'error')</script>";
            }
        }else{
            echo "<script>Swal.fire('使用者不存在', '', 'error')</script>";
        }
    }
    // echo '<script>Swal.fire("帳號:' . $username . '，密碼:' . $password . '", "", "error")</script>';
}
?>
