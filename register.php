<?php
    // Initialize the session
    if(!isset($_SESSION)) { 
        session_start(); 
    }
    
    // Check if the user is already logged in, if yes then redirect him to welcome page
    if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
        header("location: score.php");
        exit;
    }
    
    // Include config file
    require_once "config.php";

    // Define variables and initialize with empty values
    $username = $password = $nickname = $confirm_password = "";
?>
<html>
    <head>
        <title>期末成績計算系統</title>
        <link rel="icon" href="images/favicon.ico" type="image/ico">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/home.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body style="background-image: url('images/background.jpg') !important; background-size: cover !important;">
        <?php
            require_once "nav.php";
        ?>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first mt-5">
                    <h1>註冊</h1>
                </div>

                <!-- Login Form -->
                <form name="form2" method="POST">
                    <input type="number" id="username" class="fadeIn first" name="username" placeholder="請輸入學號">
                    <input type="text" id="nickname" class="fadeIn second" name="nickname" placeholder="請輸入姓名">
                    <input type="password" id="password" class="fadeIn third" name="password" placeholder="請輸入密碼">
                    <input type="password" id="confirm_password" class="fadeIn fourth" name="confirm_password" placeholder="請再次輸入密碼">
                    <input type="submit" class="fadeIn fourth" value="註冊">
                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                <a class="underlineHover" href="home.php">已經註冊了? 點我回登入頁</a>
            </div>

        </div>
    </div>
    </body>
</html>
<?php
if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty(trim($_POST["username"])) && empty(trim($_POST["nickname"])) && empty(trim($_POST["password"])) && empty(trim($_POST["confirm_password"]))){
        echo '<script>Swal.fire("每個欄位都必須填寫", "", "error")</script>';
    } else{
        $username = trim($_POST["username"]);
        $nickname = trim($_POST["nickname"]);
        $password = trim($_POST["password"]);
        $confirm_password = trim($_POST["confirm_password"]);
    }

    if($password != $confirm_password){
        echo '<script>Swal.fire("密碼與確認密碼不符", "", "error")</script>';
    }

    if(isset($username) && isset($password)){
        $sql = "SELECT * From `users` WHERE `username` = $username";
        $result = mysqli_query($link, $sql);
        if(mysqli_num_rows($result) > 0){
            echo '<script>Swal.fire("此帳號已被註冊", "", "error")</script>';
        }else{
            $insert_sql = "INSERT INTO `users`(`username`, `nickname`, `password`) VALUES ('$username','$nickname','$password')";
            mysqli_query($link, $insert_sql);
            echo '<script>Swal.fire("註冊成功", "", "success")</script>';

            // Redirect user to score page
            header("location: score.php");
        }
    }

}
?>
