
<?php
    // Initialize the session
    if(!isset($_SESSION)) { 
        session_start(); 
    }
    
    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: home.php");
        // echo '<script>Swal.fire("尚未登入", "", "error")</script>';
        exit;
    }

    // Include config file
    require_once "config.php";
?>
<html>
    <head>
        <title>期末成績計算系統</title>
        <link rel="icon" href="images/favicon.ico" type="image/ico">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="css/home.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </head>
    <body style="background-image: url('images/background.jpg') !important; background-size: cover !important;">
        <?php
            require_once "nav.php";
        ?>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first mt-5">
                    <h1>成績輸入</h1>
                </div>

                <!-- Login Form -->
                <form name="form3" method="POST">
                    <input type="number" id="usual_percent" class="fadeIn first" name="usual_percent" placeholder="請輸入平時占比">
                    <input type="number" id="midterm_percent" class="fadeIn second" name="midterm_percent" placeholder="請輸入期中占比">
                    <input type="number" id="usual_score" class="fadeIn third" name="usual_score" placeholder="請輸入平時成績">
                    <input type="number" id="midterm_score" class="fadeIn fourth mb-3" name="midterm_score" placeholder="請輸入期中成績">
                    <style>
                        .form-control{
                            width: 85% !important;
                            margin-left: auto;
                            margin-right: auto;
                        }
                    </style>
                    <select class="form-control my-3" name="class">
                        <?php
                            $sql = "SELECT * From `class`";
                            $result = mysqli_query($link, $sql);
                            $row = $result->fetch_all();
                            if(mysqli_num_rows($result) > 0){
                                foreach ($row as $r) {
                                    $value = $r;
                                    $selectedId = $value[0];
                                    echo "<option value='$selectedId'>".$value[1]."-".$value[2]."</option>";
                                }
                            }
                        ?>
                    </select>
                    <div class="row mt-2">
                        <div class="col-6">
                            <input type="reset" class="fadeIn fourth" style="margin: unset !important;" value="重設">
                        </div>
                        <div class="col-6">
                            <input type="submit" class="fadeIn fourth" style="margin: unset !important;" value="計算">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
<?php
$usual_percent = $midterm_percent = $usual_score = 0;
$midterm_score = $expected_final_percent = $expected_final_score = 0;
$class = 0;
$userId = $_SESSION["id"];

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty(trim($_POST["usual_percent"])) && empty(trim($_POST["midterm_percent"])) && empty(trim($_POST["usual_score"])) && empty(trim($_POST["midterm_score"])) && empty(trim($_POST["class"]))){
        echo '<script>Swal.fire("每個欄位都必須填寫", "", "error")</script>';
    }else{
        $usual_percent = (int)trim($_POST["usual_percent"]);
        $midterm_percent = (int)trim($_POST["midterm_percent"]);
        $usual_score = (int)trim($_POST["usual_score"]);
        $midterm_score = (int)trim($_POST["midterm_score"]);
        $class = (int)trim($_POST["class"]);
        $_SESSION["class"] = $class;

        $expected_final_percent = 100 - $usual_percent - $midterm_percent;
        $expected_final_score = (60 - ($usual_score * $usual_percent / 100) - ($midterm_score * $midterm_percent / 100)) / ($expected_final_percent/100);
        $expected_final_score = intval($expected_final_score);
        if($expected_final_score > 0){
            echo "<script>Swal.fire('預估期末分數: $expected_final_score', '', 'success')</script>";
            $sql = "SELECT * From `score` WHERE `userId` = $userId AND `classId` = $class";
            $result = mysqli_query($link, $sql);
            if(mysqli_num_rows($result) > 0){
                $update_sql = "UPDATE `score` SET `usual_percent`=$usual_percent,`midterm_percent`=$midterm_percent,`expected_final_percent`=$expected_final_percent,`usual_score`=$usual_score,`midterm_score`=$midterm_score,`expected_final_score`=$expected_final_score WHERE `userId` = $userId AND `classId` = $class";
                mysqli_query($link, $update_sql);
                echo "<script>Swal.fire('已儲存並修改', '', 'success')</script>";
                // Redirect user to result page
                header("location: result.php");
            }else{
                $insert_sql = "INSERT INTO `score`(`usual_percent`, `midterm_percent`, `expected_final_percent`, `usual_score`, `midterm_score`, `expected_final_score`, `userId`, `classId`) VALUES ($usual_percent,$midterm_percent,$expected_final_percent,$usual_score,$midterm_score,$expected_final_score,$userId,$class)";
                mysqli_query($link, $insert_sql);
                echo "<script>Swal.fire('已儲存', '', 'success')</script>";
                // Redirect user to result page
                header("location: result.php");
            }
        }else{
            $sql = "SELECT * From `score` WHERE `userId` = $userId AND `classId` = $class";
            $result = mysqli_query($link, $sql);
            if(mysqli_num_rows($result) > 0){
                $update_sql = "UPDATE `score` SET `usual_percent`=$usual_percent,`midterm_percent`=$midterm_percent,`expected_final_percent`=$expected_final_percent,`usual_score`=$usual_score,`midterm_score`=$midterm_score,`expected_final_score`=0 WHERE `userId` = $userId AND `classId` = $class";
                mysqli_query($link, $update_sql);
                echo "<script>Swal.fire('已儲存並修改', '', 'success')</script>";
                // Redirect user to result page
                header("location: result.php");
            }else{
                $insert_sql = "INSERT INTO `score`(`usual_percent`, `midterm_percent`, `expected_final_percent`, `usual_score`, `midterm_score`, `expected_final_score`, `userId`, `classId`) VALUES ($usual_percent,$midterm_percent,$expected_final_percent,$usual_score,$midterm_score,0,$userId,$class)";
                mysqli_query($link, $insert_sql);
                echo "<script>Swal.fire('已儲存', '', 'success')</script>";
                // Redirect user to result page
                header("location: result.php");
            }
            echo "<script>Swal.fire('你已通過', '', 'success')</script>";
        }
        // $sql = "SELECT * From `score` WHERE `userId` = $userId";
    }
}
?>