<?php
    // Initialize the session
    if(!isset($_SESSION)) { 
        session_start(); 
    }
?>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <a class="navbar-brand h1" href="home.php">期末成績計算系統</a>
    <div class="navbar-text">
        <?php
            if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]){
                $username = $_SESSION["username"];
                $name = $_SESSION["nickname"];
                echo "<a class='navbar-brand h1' href='#'>嗨~ $name 您好 </a>";
                echo "<a class='navbar-brand h1 btn btn-info' href='query.php'>查詢成績</a>";
                echo "<a class='navbar-brand h1 btn btn-info' href='score.php'>計算成績</a>";
                echo "<a class='navbar-brand h1 btn btn-info' href='logout.php'>登出</a>";
            }else{
                echo "<a class='navbar-brand h1 btn btn-info' href='home.php'>登入</a>";
            }
        ?>
    </div>
</nav>